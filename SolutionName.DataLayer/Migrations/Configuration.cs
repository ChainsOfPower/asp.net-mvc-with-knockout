namespace SolutionName.DataLayer.Migrations
{
    using System.Data.Entity.Migrations;
    using Model;
    using SolutionName.DataLayer;

    internal sealed class Configuration : DbMigrationsConfiguration<SalesContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(SalesContext context)
        {
            context.SalesOrders.AddOrUpdate(
                so => so.CustomerName,
                new SalesOrder
                {
                    CustomerName = "Adam",
                    PONumber = "9876",
                    SalesOrderItems =
                    {
                        new SalesOrderItem {ProductCode ="ABC", Quantity = 10, UnitPrice = 1.23m },
                        new SalesOrderItem {ProductCode ="XYZ", Quantity = 7, UnitPrice = 14.53m },
                        new SalesOrderItem {ProductCode ="SAMPLE", Quantity = 100, UnitPrice = 12.23m },
                    }
                },
                new SalesOrder { CustomerName = "Michael"},
                new SalesOrder { CustomerName = "David", PONumber = "Acme 9" },
                new SalesOrder { CustomerName = "Netko", PONumber = "Ez katka" }
                );
        }
    }
}
