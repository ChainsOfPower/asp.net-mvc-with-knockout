﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SolutionName.DataLayer;
using SolutionName.Model;

namespace SolutionName.Web.Controllers
{
    public class SalesOrderItemsController : ApiController
    {
        private SalesContext db = new SalesContext();

        // GET: api/SalesOrderItems
        public IQueryable<SalesOrderItem> GetSalesOrderItems()
        {
            return db.SalesOrderItems;
        }

        // GET: api/SalesOrderItems/5
        [ResponseType(typeof(SalesOrderItem))]
        public IHttpActionResult GetSalesOrderItem(int id)
        {
            SalesOrderItem salesOrderItem = db.SalesOrderItems.Find(id);
            if (salesOrderItem == null)
            {
                return NotFound();
            }

            return Ok(salesOrderItem);
        }

        // PUT: api/SalesOrderItems/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSalesOrderItem(int id, SalesOrderItem salesOrderItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != salesOrderItem.SalesOrderItemId)
            {
                return BadRequest();
            }

            db.Entry(salesOrderItem).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SalesOrderItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SalesOrderItems
        [ResponseType(typeof(SalesOrderItem))]
        public IHttpActionResult PostSalesOrderItem(SalesOrderItem salesOrderItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SalesOrderItems.Add(salesOrderItem);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = salesOrderItem.SalesOrderItemId }, salesOrderItem);
        }

        // DELETE: api/SalesOrderItems/5
        [ResponseType(typeof(SalesOrderItem))]
        public IHttpActionResult DeleteSalesOrderItem(int id)
        {
            SalesOrderItem salesOrderItem = db.SalesOrderItems.Find(id);
            if (salesOrderItem == null)
            {
                return NotFound();
            }

            db.SalesOrderItems.Remove(salesOrderItem);
            db.SaveChanges();

            return Ok(salesOrderItem);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SalesOrderItemExists(int id)
        {
            return db.SalesOrderItems.Count(e => e.SalesOrderItemId == id) > 0;
        }
    }
}