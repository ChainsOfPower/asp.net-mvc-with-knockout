﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using SolutionName.DataLayer;
using SolutionName.Model;
using SolutionName.Web.ViewModels;
using System;
using System.Data.Entity.Infrastructure;

namespace SolutionName.Web.Controllers
{
    public class SalesController : Controller
    {
        private SalesContext db;

        public SalesController()
        {
            db = new SalesContext();
        }

        // GET: Sales
        public ActionResult Index()
        {
            return View(db.SalesOrders.ToList());
        }

        // GET: Sales/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SalesOrder salesOrder = db.SalesOrders.Find(id);
            if (salesOrder == null)
            {
                return HttpNotFound();
            }

            SalesOrderViewModel salesOrderViewModel = 
                ViewModels.Helpers.CreateSalesOrderViewModelFromSalesOrder(salesOrder);
            salesOrderViewModel.MessageToClient = "I originated from the viewmodel, rather than the model";

            return View(salesOrderViewModel);
        }

        // GET: Sales/Create
        public ActionResult Create()
        {
            SalesOrderViewModel salesOrderViewModel = new SalesOrderViewModel();
            salesOrderViewModel.ObjectState = ObjectState.Added;
            return View(salesOrderViewModel);
        }

        

        // GET: Sales/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SalesOrder salesOrder = db.SalesOrders.Find(id);
            if (salesOrder == null)
            {
                return HttpNotFound();
            }

            SalesOrderViewModel salesOrderViewModel = 
                ViewModels.Helpers.CreateSalesOrderViewModelFromSalesOrder(salesOrder);
            salesOrderViewModel.MessageToClient = string.Format("The original value of Customer Name is {0}.", salesOrderViewModel.CustomerName);

            return View(salesOrderViewModel);
        }

        

        // GET: Sales/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SalesOrder salesOrder = db.SalesOrders.Find(id);
            if (salesOrder == null)
            {
                return HttpNotFound();
            }
            

            SalesOrderViewModel salesOrderViewModel = 
                ViewModels.Helpers.CreateSalesOrderViewModelFromSalesOrder(salesOrder);
            salesOrderViewModel.ObjectState = ObjectState.Deleted;

            salesOrderViewModel.MessageToClient = "You are about to permanently delete this sales order.";

            return View(salesOrderViewModel);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HandleModelStateException]
        public JsonResult Save(SalesOrderViewModel salesOrderViewModel)
        {
            if(!ModelState.IsValid)
            {
                throw new ModelStateException(ModelState);
            }

            SalesOrder salesOrder = ViewModels.Helpers.CreateSalesOrderFromSalesOrderViewModel(salesOrderViewModel);
            if (salesOrder.ObjectState == ObjectState.Deleted)
                salesOrder.SalesOrderItems.Clear();

            foreach(int salesOrderItemId in salesOrderViewModel.SalesOrderItemsToDelete)
            {
                SalesOrderItem salesOrderItem = db.SalesOrderItems.Find(salesOrderItemId);
                if (salesOrderItem != null)
                    salesOrderItem.ObjectState = ObjectState.Deleted;
            }

            db.SalesOrders.Attach(salesOrder);
            db.ApplyStateChanges();
            string messageToClient = string.Empty;

            try
            {
                db.SaveChanges();
            }
            catch(DbUpdateConcurrencyException)
            {
                messageToClient 
                    = "Someone else has modified this sales order since you retrieved it. Your changes have not been applied. What you see now are the current values in the database.";
            }
            catch (Exception ex)
            {
                throw new ModelStateException(ex);
            }
            

            if(salesOrder.ObjectState == ObjectState.Deleted)
            {
                return Json(new { newLocation = "/Sales/Index/" });
            }

            if(messageToClient.Trim().Length == 0)
                messageToClient = ViewModels.Helpers.GetMessageToClient(salesOrderViewModel.ObjectState, salesOrderViewModel.CustomerName);

            salesOrderViewModel.SalesOrderId = salesOrder.SalesOrderId;
            db.Dispose();
            db = new SalesContext();
            salesOrder = db.SalesOrders.Find(salesOrderViewModel.SalesOrderId);

            salesOrderViewModel = ViewModels.Helpers.CreateSalesOrderViewModelFromSalesOrder(salesOrder);
            salesOrderViewModel.MessageToClient = messageToClient;

            return Json(new { salesOrderViewModel });
        }
        
    }
}
